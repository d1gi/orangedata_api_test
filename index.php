<?php

use orangedata\orangedata_client;

require_once 'vendor/autoload.php';

$sign_pkey = getcwd() . '\secure_path\private_key.pem'; //path to private key for signing
$ssl_client_key = getcwd() . '\secure_path\client.key'; //path to client private key for ssl
$ssl_client_crt = getcwd() . '\secure_path\client.crt'; //path to client certificate for ssl
$ssl_ca_cert = getcwd() . '\secure_path\cacert.pem'; //path to cacert for ssl
$ssl_client_crt_pass = 1234; //password for client certificate for ssl
$inn = '0123456789'; //ИНН

$api_url = "https://apip.orangedata.ru:2443";

//$data = json_decode('{"id":"2_loc_z5bYWHvD","inn":"1234567890","group":"Main","key":"1234567890","content":{"type":1,"positions":[{"quantity":2.0,"price":10.0,"tax":1,"text":"\u0421\u0430\u043c\u043e\u0432\u044b\u0432\u043e\u0437 ru","paymentMethodType":4,"paymentSubjectType":1},{"quantity":1.0,"price":0.0,"tax":1,"text":"\u0414\u043e\u0441\u0442\u0430\u0432\u043a\u0430","paymentMethodType":4,"paymentSubjectType":4}],"checkClose":{"payments":[{"type":2,"amount":20.0}],"taxationSystem":0},"customerContact":"user@domain.com"}}', true);
//var_dump($data);

$byer = new orangedata\orangedata_client($inn, $api_url, $sign_pkey, $ssl_client_key, $ssl_client_crt, $ssl_ca_cert, $ssl_client_crt_pass);

$byer->is_debug(true);

try {
    //create client new order, add positions , add payment, send request
    $byer->create_order('23423423434', 1, 'example@example.com', 1)
        ->add_position_to_order(6.123456, '10.', 1, 'matches', 1, 10)
        ->add_position_to_order(7, 10, 1, 'matches2', null, 10)
        ->add_position_to_order(345., 10.76, 1, 'matches3', 3, null)
        ->add_payment_to_order(1, 131.23)
        ->add_payment_to_order(2, 3712.2)
        ->add_agent_to_order(127,['+79998887766', '+76667778899'], 'Operation', ['+79998887766'], ['+79998887766'], 'Name', 'ulitsa Adress, dom 7', 3123011520, ['+79998887766', '+76667778899'])
        ->add_user_attribute('Любимая цитата', 'В здоровом теле здоровый дух, этот лозунг еще не потух!');

    //view response
    $result = $byer->send_order();
    var_dump($result);
} catch (Exception $ex) {
    echo 'Ошибка:' . PHP_EOL . $ex->getMessage();
}

//view status of order
/*
try {
    $order_status = $byer->get_order_status(23423423434);
    var_dump($order_status);
} catch (Exception $ex) {
    echo 'Ошибка:' . PHP_EOL . $ex->getMessage();
}
*/
echo "\n";
