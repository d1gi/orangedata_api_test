<?php

// autoload_psr4.php @generated by Composer

$vendorDir = dirname(dirname(__FILE__));
$baseDir = dirname($vendorDir);

return array(
    'orangedata\\' => array($vendorDir . '/orangedata/orangedata-client'),
);
