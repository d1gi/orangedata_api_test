<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInitda383d9bb1da0199d11e32362c3e079d
{
    public static $prefixLengthsPsr4 = array (
        'o' => 
        array (
            'orangedata\\' => 11,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'orangedata\\' => 
        array (
            0 => __DIR__ . '/..' . '/orangedata/orangedata-client',
        ),
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInitda383d9bb1da0199d11e32362c3e079d::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInitda383d9bb1da0199d11e32362c3e079d::$prefixDirsPsr4;

        }, null, ClassLoader::class);
    }
}
